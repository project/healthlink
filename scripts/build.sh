#!/usr/bin/env bash
set -e

#
# Script to build the Healthlink Demo distribution
#
# Usage: scripts/build.sh <destination> from the distribution main directory
#

usage () {
  echo "Usage: build.sh <DESTINATION_PATH>" >&2
  exit 1
}

# Set destination and current directory
DESTINATION=$1
CURDIR=`pwd -P`

# Make sure destination is set
if [ "x$DESTINATION" == "x" ]; then
  usage
fi

# Make sure we're in the correct directory
if [ ! -f "build-healthlink.make.yml" ]; then
  usage
fi

# Remove current drupal dir
if [ -e "$DESTINATION" ]; then
  echo "Removing existing destination: $DESTINATION ..."
  rm -rf $DESTINATION
  echo "Done."
fi

# Perform build
# Build Drupal Core
MAKEFILE='build-healthlink.make.yml'
DRUSH_OPTS='--working-copy --no-patch-txt --force-complete --concurrency=5'
echo 'Running drush make...'
drush make $DRUSH_OPTS "$CURDIR/$MAKEFILE" "$DESTINATION"
# Build Drupal Contrib modules
MAKEFILE='drupal-org.make.yml'
DRUSH_OPTS='--working-copy --no-patch-txt --no-core --concurrency=5'
drush make $DRUSH_OPTS "$CURDIR/$MAKEFILE" "$DESTINATION"
# Remove Demo Framework .gitignore
if [ -e "$DESTINATION/profiles/df/.gitignore" ]; then
  rm "$DESTINATION/profiles/df/.gitignore"
fi

# Copy Demo Framework Scenario and Custom theme folders to the
# Demo Framework directory
SCENARIO='dfs_dfhc'
DEST_PATH="$DESTINATION/profiles/df/modules/dfs/"
cp -R $SCENARIO $DEST_PATH
THEME='healthdemo'
DEST_PATH="$DESTINATION/profiles/df/themes/"
cp -R $THEME $DEST_PATH
