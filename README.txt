Health Link Distribution Instructions

Getting Started
---------------

Download and build the files for Healthlink using the script
"./build.sh path-to-web-server". For example, if the root of your webserver
is in /htdocs/drupal, then you would use "./build.sh /htdocs/drupal".
This will build Healthlink using the latest -dev files from drupal.org. This script requires
the "drush" command and uses "drush make" to download and build all of the
dependencies for Healthlink. You should be using at least Drush 5.x.

Installation Instructions
-------------------------

To install Healthlink the first time, you will run the "install.php" script
from your web browser. NOTE when using a hosting company such as Pantheon, it
will run install.php for you. The Install script will take you through
multiple pages of steps. There is very little difference between this process
and a normal Drupal site installation, except that there is an additional page
for choosing a Demo Framework Scenario.

Enable Healthlink Demo Framework
--------------------------------

During the Install script, you will be given an option to choose a Demo Framework
scenario. Choose the Healthlink scenario at this time. It will takes a few minutes
to install. If you do not choose Healthlink during the Install script, you will be
given the chance to choose it after the Install script has finished. You can also
choose or reset the Healthlink Demo Framework any time at admin/df.